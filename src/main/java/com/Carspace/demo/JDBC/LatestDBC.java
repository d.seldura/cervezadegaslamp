package com.Carspace.demo.JDBC;

import java.sql.*;
import ch.qos.logback.core.db.dialect.DBUtil;
import com.Carspace.demo.Entities.StatusLog;
import org.apache.commons.dbutils.DbUtils;
import com.Carspace.demo.Entities.Latest;

public class LatestDBC {
    private Connection connection;
    private ResultSet result;
    private PreparedStatement ps;
    private Statement statement;

    public LatestDBC() {
        try {
            //System.out.println("LatestDBC");
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            this.connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/statuslog", "JustinCider", "miakhalifa");
            statement = connection.createStatement();
            String createResponseTable = "create table if not exists latest("
                    + "spaceNo int NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                    + "vacancy VARCHAR(20),"
                    + "date_time TIMESTAMP)";
            statement.execute(createResponseTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Latest getLatest(int index) {
        Latest queryResult = new Latest();
        try {
            //System.out.printf("LatestDBC.getLatest");
            statement = connection.createStatement();
            result = statement.executeQuery("SELECT * FROM statuslog.parkingstatus where spaceNo = " + index + "  order by date_time DESC limit 1");
            while (result.next()) {
                queryResult.setSpaceNo(result.getString("spaceNo"));
                queryResult.setVacancy(result.getString("Vacancy"));
                queryResult.setDateTime(result.getString("date_time"));
            }
            System.out.println(queryResult.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return queryResult;
    }

    public boolean updateLatest(Latest status) {
        try {
            //System.out.println("LatestDBC.updateLatest");
            String command = "update latest SET "
                    + "vacancy = ?,"
                    + "date_time = ?"
                    + " WHERE spaceNo = ?";
            ps = connection.prepareStatement(command);
            ps.setString(1, status.getVacancy());
            ps.setString(2, status.getDateTime());
            ps.setString(3, status.getSpaceNo());
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public String refreshStatus() {
        Latest temp = new Latest();
        String outputMessage = "";
        try {
            //System.out.println("LatestDBC.refreshStatus");
            for (int i=1; i<=4; i++){
                temp = getLatest(i);
                updateLatest(temp);
                if (i!=4)
                    outputMessage += temp.getVacancy() + ",";
                else
                    outputMessage +=temp.getVacancy();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(connection, statement, result);
        }
        return outputMessage;
    }

}