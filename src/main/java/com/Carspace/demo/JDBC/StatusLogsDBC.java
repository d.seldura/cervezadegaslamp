package com.Carspace.demo.JDBC;

/**
 * Created by danie on 3/2/2019.
 */
import java.sql.*;
import ch.qos.logback.core.db.dialect.DBUtil;
import com.Carspace.demo.Entities.StatusLog;
import org.apache.commons.dbutils.DbUtils;

public class StatusLogsDBC {
    private Connection connection;
    private ResultSet result;
    private PreparedStatement ps;
    private Statement statement;

    public StatusLogsDBC(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/statuslog","JustinCider","miakhalifa");
            statement = connection.createStatement();
            String createResponseTable = "create table if not exists parkingstatus("
                    + "logNo int NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                    + "spaceNo int,"
                    + "vacancy VARCHAR(20),"
                    + "date_time TIMESTAMP)";
            statement.execute(createResponseTable);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean updateParkingStatus(StatusLog status){
        try{
            String command = "insert into parkingStatus("
                    + "spaceNo,"
                    + "vacancy,"
                    + "date_time)"
                    + "values (?,?,CURRENT_TIMESTAMP)";
            ps = connection.prepareStatement(command);
            ps.setString(1, status.getSpaceNo());
            ps.setString(2, status.getVacancy());
            ps.execute();
        }catch(Exception e){
            e.printStackTrace();
            return false;
         }finally {
        DbUtils.closeQuietly(connection,statement,result);
        }
        return true;
    }

}

