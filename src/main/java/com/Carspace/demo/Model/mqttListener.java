package com.Carspace.demo.Model; /**
 * Created by danie on 3/2/2019.
 */
import com.Carspace.demo.JDBC.LatestDBC;
import com.Carspace.demo.JDBC.StatusLogsDBC;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import com.Carspace.demo.Entities.StatusLog;

public class mqttListener implements MqttCallback {

    /** The broker url. */
    private static final String localbrokerUrl = "tcp://127.0.0.1:1883";
    //private static final String localbrokerUrl = "tcp://192.168.43.168:1883";
    //private static final String brokerUrl = "tcp://127.0.0.1:1883";
    private static final String brokerUrl = "tcp://192.168.43.168:1883";


    public static String getBrokerUrl() {
        return brokerUrl;
    }

    /** The client id. */
    private static final String clientId = "carspace_webserver";

    /** The topic. */
    private static String topic = null;

    public mqttListener(String topic) {
        this.topic = topic;
    }

    public static String getTopic() {
        return topic;
    }

    public void subscribe() {

        MemoryPersistence persistence = new MemoryPersistence();

        try {

            MqttClient sampleClient = new MqttClient(brokerUrl, clientId, persistence);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);

            System.out.println("checking");

            System.out.println("Mqtt Connecting to broker: " + brokerUrl);
            sampleClient.connect(connOpts);
            System.out.println("Mqtt Connected");

            sampleClient.setCallback(this);
            sampleClient.subscribe(topic);

            System.out.println("Subscribed");
            System.out.println("Listening");

        } catch (MqttException me) {

            System.out.println("Mqtt reason " + me.getReasonCode());
            System.out.println("Mqtt msg " + me.getMessage());
            System.out.println("Mqtt loc " + me.getLocalizedMessage());
            System.out.println("Mqtt cause " + me.getCause());
            System.out.println("Mqtt exception " + me);
        }
    }
    public void connectionLost(Throwable arg0) {

    }

    public void deliveryComplete(IMqttDeliveryToken arg0) {

    }
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        //update db
        //insert the values retrieved from mqqt to an object
        StatusLog ps = new StatusLog();
        String temp = message.toString();
        String[] data = temp.split(":");
        ps.setSpaceNo(data[0]);
        if (data[1].equalsIgnoreCase("1"))
            ps.setVacancy("true");
        else
            ps.setVacancy("false");
        StatusLogsDBC psDBC = new StatusLogsDBC();
        psDBC.updateParkingStatus(ps);
        System.out.println("Mqtt topic : " + topic);
        System.out.println("Mqtt msg : " + message.toString());
        LatestDBC update = new LatestDBC();
        //Send an update command to the mqtt client
        IMqttClient publisher = new MqttClient(localbrokerUrl,"WebsiteUpdateTrigger");
        publisher.connect();
        MqttMessage msg = new MqttMessage(String.format(update.refreshStatus()).getBytes());
        msg.setQos(0);
        msg.setRetained(false);
        publisher.publish("updatePage",msg);
    }
}