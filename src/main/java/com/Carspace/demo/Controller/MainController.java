package com.Carspace.demo.Controller;

import com.Carspace.demo.Entities.Latest;
import com.Carspace.demo.JDBC.LatestDBC;
import com.Carspace.demo.Model.mqttListener;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by danie on 3/2/2019.
 */
@Controller
public class MainController {

    @RequestMapping("/index")
    public ModelAndView showIndex() {
        ModelAndView mav = new ModelAndView("index");
        mqttListener listener = new mqttListener("outTopic");
        listener.subscribe();
        return mav;
    }
}