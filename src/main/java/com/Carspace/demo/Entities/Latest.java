package com.Carspace.demo.Entities;

/**
 * Created by danie on 5/3/2019.
 */
public class Latest {
    private String Vacancy;
    private String DateTime;
    private String SpaceNo;

    public Latest() {
    }

    public String getVacancy() {
        return Vacancy;
    }

    public void setVacancy(String vacancy) {
        Vacancy = vacancy;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public String getSpaceNo() {
        return SpaceNo;
    }

    @Override
    public String toString(){
        return getSpaceNo() +"_" + getVacancy();
    }


    public void setSpaceNo(String spaceNo) {
        SpaceNo = spaceNo;
    }
}
