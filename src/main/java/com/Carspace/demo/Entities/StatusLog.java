package com.Carspace.demo.Entities;

/**
 * Created by danie on 3/2/2019.
 */
public class StatusLog {
    private String spaceNo;
    private String vacancy;

    public StatusLog() {
    }

    public String getSpaceNo() {
        return spaceNo;
    }

    public void setSpaceNo(String spaceNo) {
        this.spaceNo = spaceNo;
    }

    public String getVacancy() {
        return vacancy;
    }

    public void setVacancy(String vacancy) {
        this.vacancy = vacancy;
    }
}
